## C_PLS

Curator: JQK

Purpose: Calculate dimensionality PLS v1 preprocesssing data. Includes creation of common cords map.

Run locally. Note that if run on tardis, many of the paths will be incorrect when working on the server and would have to be adjusted.

In general, scripts have been run separately for YAs and OAs.

Scripts

S0_collectInputData

- N = 43 YAs (1126 no rest)
- Copy v1 preprocessed data to data directory

S1_create_batchfile_STSWD_rest

- Navigate to this file using the Terminal and execute it
- Use the template (has to be manually created and filled with relevant parameters) and create individual batch files by inserting correct paths etc.

S2_make_meanbold_datamat_restingstate

- PLS GUI will automatically create data matrices based on the mean BOLD signatures; this should run automatically if the batchfiles include the correct information.
- In other analyses, the resulting files may rather be named ‘task_’ to separate them from the mean files that will have GM-masking for the final PLS. Note that the data in the current structures are not necessarily what we are looking for (e.g. mask-wise).

X1_createIndividual_mean_sd_cov_nii

- At this point, it makes sense to create individual summary images of BOLD mean, SD and COV (coefficient of variation). That way, individual data can be inspected and potential problems identified. This also reduces the need to load the whole data again in case e.g. different mask should be used for the analysis. This step was not incorporated originally and therefore has a different naming convention. Note that this step can be performed prior to any other step for conditions with one block, for data with multiple blocks, the task-matrices are usually loaded to get the condition information.

S3_commonGMvoxels_STSWD

- Extract coordinates of voxels with non-zero power across subjects. Different coordinate selections are available:
o final_coords: non-NaN GM voxels across subjects
o final_coords_withoutZero: non-NaN & non-zero GM voxels across subjects
o GM_coords
o nii_nonZero: non-zero coordinates
o nii_coords: original coordinates

S3B_commonGMvoxels_summary_STSWD

- This script can be used as an alternative to S3_ if X1 has been executed. This will load the SD summary images and therefore circumvent the long loading procedures.

S4_create_sdbold_STSWD_rest

- Create SD BOLD matrices on the basis of the mean matrices.
- Use final_coords_withoutZero.

S4B_changePathsinSDmat

- Correct the paths to the STD mat files (may not always be necessary)

S5_runPLS_rest

- Manually copy relevant parameters to template file
- Run PLS analysis using the GUI
