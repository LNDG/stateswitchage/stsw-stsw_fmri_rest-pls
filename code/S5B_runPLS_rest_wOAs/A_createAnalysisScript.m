% This script extracts the necessary infos for a PLS analysis of
% dimensionality. Unfortunately, text files are hard to edit, so I rely on
% manual editing in the end. The necessary information is output in the
% command window and can be directly copied to replace the placeholders.

% 180202 | written by JQK
% 180228 | adpated for STSWD YA from Merlin2 phycaa+
% 180326 | include OAs

clear all; clc;

%% subject list

% N = 43 YA (1126 has no Rest data) + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% get necessary info for template

% load dimensionality data across subjects

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.dimData      = [pn.root, 'analyses/B_dimensionality/B_data/D_Dimensionality_v1/'];
pn.SDdataDir    = [pn.root, 'analyses/C_PLS/B_data/SD_STSWD_v1/'];

load([pn.dimData, 'C_DimSubjectwise_GSR_noGSR_N96_GM.mat'], 'dimensionality', 'dimensionality_GSR', 'categories', 'criterions', 'IDs');

for indID = 1:numel(IDs)
    % extract dimensionality estimate of interest for each subject
    curDim{indID,1} = squeeze(dimensionality(1,9,indID));
    dataname{indID,1} = 'behavior_data';
    % extract names of SD datamats
    groupfiles{indID,1} = ['std_', IDs{indID}, '_restingstate_BfMRIsessiondata.mat'];
end

% load template (has to be copied into directory manually)

textfileInput = 'n96_BehavPLS_SDBOLD_PCAdim_90_SpatCorwoGSA_fMRIanalysis_v4';
pn.template = [pn.SDdataDir, textfileInput];

% create info to insert into template

Fillin.RESULTFILE = {[textfileInput(1:end-16), '_BfMRIresult.mat']};
Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = curDim;

%% Output necessary information, has to be manually copied into .txt file

fprintf('%s ', Fillin.RESULTFILE{1});

for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

for indSub = 1:numel(IDs) 
    fprintf('%s    %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub});
end
