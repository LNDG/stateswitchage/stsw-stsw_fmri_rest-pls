#!/bin/bash

# For STSWD Rest, create PLS info files from template.
# Note sed adaptations for MacOS: http://www.markhneedham.com/blog/2011/01/14/sed-sed-1-invalid-command-code-r-on-mac-os-x/

batchOut="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/B_data/SD_STSWD_v1"
templateIn="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/B_data/template"

#N43
#subjectID="1117 1118 1120 1124 1125 1131 1132 1135 1136 1151 1160 1164 1167 1169 1172 1173 1178 1182 1214 1215 1216 1219 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281";
#N53
subjectID="2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261"

for SID in $subjectID; do
    
	cp ${templateIn}/PLS_STSWDrest_template_batch_file.txt ${batchOut}/${SID}_rest_PLS_info.txt
	old_1="dummyID"
	new_1="${SID}"
	old_2="dummy_location"
	niiLocation="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/B_data/BOLDin"
	new_2="${niiLocation}/${SID}_rest_feat_detrended_bandpassed_manualdenoise_MNI3mm.nii"
	
	# For MAC:
	sed -i "" "s|${old_1}|${new_1}|g" ${batchOut}/${SID}_rest_PLS_info.txt
	sed -i "" "s|${old_2}|${new_2}|g" ${batchOut}/${SID}_rest_PLS_info.txt
	
	# For UNIX:
	#sed -i "s|${old_1}|${new_1}|g" ${batchOut}/${SID}_rest_PLS_info.txt
	#sed -i "s|${old_2}|${new_2}|g" ${batchOut}/${SID}_rest_PLS_info.txt
				
done