function X3_createIndividualSDBOLD()

% create SDBOLD niftys from the PLS matrices

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.tools	= [pn.root, 'analyses/C_PLS/T_tools/'];  addpath(genpath(pn.tools));
pn.data     = [pn.root, 'analyses/C_PLS/B_data/'];
pn.X1       = [pn.data, 'X1_IndividualSD/']; mkdir(pn.X1);
pn.X2       = [pn.data, 'X2_IndividualMean/']; mkdir(pn.X2);
pn.X3       = [pn.data, 'X3_IndividualCOV/']; mkdir(pn.X3);
MATPATH     = ([pn.data,'SD_STSWD_v1/']);
NIIPATH     = ([pn.data,'BOLDin/']);
COORDPATH   = [pn.data, 'VoxelOverlap/'];

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};


for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}, '.']);
    curFilename = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/B_data/SD_STSWD_v1/std_',IDs{indID},'_restingstate_BfMRIsessiondata'];
    try
        curData = load(curFilename);
        %% SD BOLD
        dataMat_SD = curData.st_datamat;        
        % create individual Nifty file
        tempNii = load_nii([COORDPATH, 'GM_MNI3mm_mask.nii']);
        tempNii.img = zeros(60,72,60);
        tempNii.img(curData.st_coords) = dataMat_SD;
        %reshape(dataMat_SD,60,72,60); % reshape to 3D matrix
        save_nii(tempNii,[pn.X1, 'SD_PLS_',IDs{indID},'.nii'])
        disp ([IDs{indID} ' done!'])
    catch
        disp ([IDs{indID} ' error!'])
    end
end