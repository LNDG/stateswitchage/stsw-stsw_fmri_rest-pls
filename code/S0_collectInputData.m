% S_0_collectInputData
% prepare tardis analysis; run locally
% This script copies the preprocessed files from the preproc directory to
% the input directory for the PLS analysis.

% 171218 | written by JQK for Merlin2 rest
% 180221 | function adapted from MerlinDynamics for STSWD YA

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.niipath          = [pn.root, 'preproc/B_data/D_preproc/'];
pn.BOLDpath         = [pn.root, 'analyses/C_PLS/B_data/BOLDin/']; mkdir(pn.BOLDpath); % path for all individual images; should be deleted after processing

% % N = 43 (1126 has no Rest data)
% IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';'1151';...
% '1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';...
%     '1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
%     '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';...
%     '1276';'1281'};

% N = 53 OAs (excluding pilots);
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indID = 1:numel(IDs)
    disp(num2str(indID));
    ID = IDs{indID};
    if ~exist(pn.BOLDpath)
        mkdir(pn.BOLDpath);
    end
    fileName = [ID, '_rest_feat_detrended_bandpassed_manualdenoise_MNI3mm.nii.gz'];
    gunzip([pn.niipath, ID, '/preproc/rest/',fileName], [pn.BOLDpath]);
end