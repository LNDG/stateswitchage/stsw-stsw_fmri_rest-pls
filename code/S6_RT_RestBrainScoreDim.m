%% Correlation: between-person: change in RT 4-1 -- change in brain score SD BOLD 4-1

%% load RTs (MRI)

% subject x attribute x dimensionality

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SummaryData_N102.mat'], 'SummaryData', 'IDs_all');

%% load brain scores

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/B_data/SD_STSWD_v1/n96_BehavPLS_SDBOLD_PCAdim_90_SpatCorwoGSA_v4_fMRIresult.mat')

IDs_BS = cellfun(@(x) x(5:8), subj_name, 'UniformOutput', 0)';
BrainScores = result.usc(:,1);

% find overlap between IDs_all and IDs_BS
[x, i_IDs_all, i_IDs_BS] = intersect(IDs_all, IDs_BS);

% scatterplot of brainscore (LV loading) and RT

RTsbyCond = squeeze(nanmean(SummaryData.MRI.RTs_md(i_IDs_all,:,:),2));

figure; scatter(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores(i_IDs_BS,:))
[r, p] = corrcoef(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores(i_IDs_BS,:))

AccbyCond = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,:,:),2));

figure; scatter(AccbyCond(:,4)-AccbyCond(:,1), BrainScores(i_IDs_BS,:))
[r, p] = corrcoef(AccbyCond(:,4)-AccbyCond(:,1), BrainScores(i_IDs_BS,:))

% No relationship for the resting state data.