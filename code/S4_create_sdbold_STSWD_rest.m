function S4_create_sdbold_STSWD_rest()
%% 2nd pre-step for PLS.
% fill matrix with standard deviations for various conditions, runs and blocks
% IDs{indID} - subject ID (string)

% NOTE: This script is expected to be run off-tardis for subjects for which
% the analysis had to be repeated.

% 171220 | adapted by JQK; use only non-zero-power GM voxels
% 180202 | adapted for repeats
% 180322 | added OAs

% N = 43 (1126 has no Rest data);  N = 53 OAs (excluding pilots);
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';...
    '1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';...
    '1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};
    
pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.tools	= [pn.root, 'analyses/C_PLS/T_tools/'];  addpath(genpath(pn.tools));
pn.data     = [pn.root, 'analyses/C_PLS/B_data/'];
MATPATH     = ([pn.data,'SD_STSWD_v1/']);
NIIPATH     = ([pn.data,'BOLDin/']);
COORDPATH   = [pn.data, 'VoxelOverlap/'];

for indID = 1:numel(IDs)
        disp(['Processing subject ', IDs{indID}, '.']);

    %% create the SDBOLD datamats

        % load subject's sessiondata file
        a = load([MATPATH, 'mean_', IDs{indID}, '_restingstate_BfMRIsessiondata.mat']);

        % load common coordinates
        load([COORDPATH, 'coords_N96.mat'], 'final_coords_withoutZero');
        final_coords = final_coords_withoutZero; % JQK: use only non-zero power values

        condition={'restingstate'}; 
        a = rmfield(a,'st_datamat');
        a = rmfield(a,'st_coords');

        %replace fields with correct info.
        a.session_info.datamat_prefix=['std_',IDs{indID},'_restingstate'];
        a.st_coords = final_coords; % constrain analysis to shared non-zero GM voxels
        a.session_info.condition=condition;
        a.session_info.condition0=condition;
        a.session_info.num_conditions=length(condition);

        for k=1:length(condition)
            a.session_info.condition_baseline0{1, k}=[-1, 1];
            a.session_info.condition_baseline{1, k}=[-1, 1];
        end

        %% create this subject's datamat

        for i = 1:a.session_info.num_runs
            fname = ([NIIPATH, a.session_info.run(i).data_files{1}]);
            % load file in 2D format
            [img] = double(S_load_nii_2d(fname));
            % constrain to non-zero-power GM voxels
            img=img(a.st_coords, :);
                % 210830: normalize to mean of 100
                img = 100*img/mean(mean(img));
                % temporal mean of this block
                img_mean = mean(img,2); % (vox) - this should be 100
                % express scans in this block as deviations from block_mean
                % and append to cond_data
                good_vox = find(img_mean);
                for t = 1:size(img,2)
                    cond_data(good_vox,t) = img(good_vox,t) - img_mean(good_vox);%must decide about perc change option here!!??
                end
            % calculate temporal STD for each voxel
            a.st_datamat(i,:) = double(squeeze(std(cond_data,0,2)))';
            clear img;
        end

        %% save individual datafile

        save([MATPATH, 'std_', IDs{indID}, '_restingstate_BfMRIsessiondata.mat'],'-struct','a','-mat');

end