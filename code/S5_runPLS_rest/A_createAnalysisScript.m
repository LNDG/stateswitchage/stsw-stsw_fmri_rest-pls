% This script extracts the necessary infos for a PLS analysis of
% dimensionality. Unfortunately, text files are hard to edit, so I rely on
% manual editing in the end. The necessary information is output in the
% command window and can be directly copied to replace the placeholders.

% 180202 | written by JQK
% 180228 | adpated for STSWD YA from Merlin2 phycaa+

clear all; clc;

%% subject list

% N = 43 (1126 has no Rest data)
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';'1151';...
    '1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';...
    '1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';...
    '1270';'1276';'1281'};

%% get necessary info for template

% load dimensionality data across subjects

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.dimData      = [pn.root, 'analyses/B_dimensionality/B_data/D_Dimensionality_v1/'];
pn.SDdataDir    = [pn.root, 'analyses/C_PLS/B_data/SD_STSWD_v1/'];

load([pn.dimData, 'C_DimSubjectwise_GSR_noGSR_GM.mat'], 'dimensionality', 'dimensionality_GSR', 'categories', 'criterions', 'IDs');

for indID = 1:numel(IDs)
    % extract dimensionality estimate of interest for each subject
    curDim{indID,1} = squeeze(dimensionality(1,9,indID));
    dataname{indID,1} = 'behavior_data';
    % extract names of SD datamats
    groupfiles{indID,1} = ['std_', IDs{indID}, '_restingstate_BfMRIsessiondata.mat'];
end

% load template (has to be copied into directory manually)

textfileInput = 'n43_BehavPLS_SDBOLD_PCAdim_90_SpatCorwoGSA_fMRIanalysis_v1';
pn.template = [pn.SDdataDir, textfileInput];

% create info to insert into template

Fillin.RESULTFILE = {[textfileInput(1:end-16), '_BfMRIresult.mat']};
Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = curDim;

%% Output necessary information, has to be manually copied into .txt file

fprintf('%s ', Fillin.RESULTFILE{1});

for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

for indSub = 1:numel(IDs) 
    fprintf('%s    %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub});
end
