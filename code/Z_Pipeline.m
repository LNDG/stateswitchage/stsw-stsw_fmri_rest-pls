% This pipeline can be used to re-create the analysis with the final
% version of the scripts. Assumes that potential functions not listed below
% have been executed manually.

% 180302 | created by JQK

pn.analysisDir = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/A_scripts/';
cd(pn.analysisDir)

%% collect input data

% S0_collectInputData

%% create task info mats

%S1_create_batchfile_STSWD_rest

%% create mean data mats

%S2_make_meanbold_datamat_rest

%% create individual summary images

% X1_createIndividual_mean_sd_cov_nii

%% create common coords

% S3_commonGMvoxels_STSWD % use if no summary images are available

% S3B_commonGMvoxels_summary_STSWD % use if summary images are available

%% create SD mats

S4_create_sdbold_STSWD_rest
