
clear all; clc;

%% process info for YAs

% N = 41 YA;
IDs = {'1117';'1118';'1120'; '1124'; '1125';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% load entropy, 1/f slopes

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat', 'info')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')
load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat'], 'mseMerged')

IDs_1f = info.IDs_all;
IDs_entropy = info.IDs_all(1:47);

indGroup = 1; indCond = 2; channels = 58:60;

% create complete ID cell

finalIDs = intersect(IDs_1f, IDs);

idx1f = find(ismember(IDs_1f, finalIDs));
idxentropy = find(ismember(IDs_entropy, finalIDs));

[finalIDs, IDs_1f(idx1f)]
[finalIDs, IDs_entropy(idxentropy)]

% PLS behavior is stacked by: group, condition, subject

numConds = 1;
numSubs = numel(finalIDs);
for indCond = 1%:numConds
for indID = 1:numSubs
    disp(['Processing ID ', finalIDs{indID}])
    groupfiles{indID,1} = ['std_',finalIDs{indID}, '_restingstate_BfMRIsessiondata.mat'];
    groupfiles_mean{indID,1} = ['task_',finalIDs{indID}, '_restingstate_BfMRIsessiondata.mat'];
    % add 1/f info
    dataForPLS{(indCond-1)*numSubs+indID,1} = squeeze(nanmean(linFit_2_30_EC(idx1f(indID),channels),2));
    % add entropy info
    dataForPLS{(indCond-1)*numSubs+indID,2} = squeeze(nanmean(nanmean(mseMerged{indGroup, 2}.MSEVanilla(idxentropy(indID),channels, 1:3),2),3));
    dataForPLS{(indCond-1)*numSubs+indID,3} = squeeze(nanmean(nanmean(mseMerged{indGroup, 1}.MSEVanilla(idxentropy(indID),channels, 1:3),2),3));
    dataForPLS{(indCond-1)*numSubs+indID,4} = squeeze(nanmean(nanmean(mseMerged{indGroup, 2}.MSEVanilla(idxentropy(indID),channels, 1:3)-mseMerged{indGroup, 1}.MSEVanilla(idxentropy(indID),channels, 1:3),2),3));
    dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

% prepare ouput for PLS file

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

for indSub = 1:numel(groupfiles)
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

for indSub = 1:numel(groupfiles_mean)
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d %d %d %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1},...
        Fillin.VALUE{indSub, 2}, Fillin.VALUE{indSub, 3}, Fillin.VALUE{indSub, 4});
end

for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1});
end

figure; scatter([Fillin.VALUE{:, 1}], [Fillin.VALUE{:, 2}])


%% add OAs

IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

IDs_1f = info.IDs_all;
IDs_entropy = info.IDs_all(48:end);

indGroup = 2; indCond = 2; channels = 58:60;

% create complete ID cell

finalIDs = intersect(IDs_1f, IDs);

idx1f = find(ismember(IDs_1f, finalIDs));
idxentropy = find(ismember(IDs_entropy, finalIDs));

[finalIDs, IDs_1f(idx1f)]
[finalIDs, IDs_entropy(idxentropy)]

% PLS behavior is stacked by: group, condition, subject

numConds = 1;
numSubs = numel(finalIDs);
for indCond = 1%:numConds
for indID = 1:numSubs
    disp(['Processing ID ', finalIDs{indID}])
    groupfiles{indID,1} = ['std_',finalIDs{indID}, '_restingstate_BfMRIsessiondata.mat'];
    groupfiles_mean{indID,1} = ['task_',finalIDs{indID}, '_restingstate_BfMRIsessiondata.mat'];
    % add 1/f info
    dataForPLS{(indCond-1)*numSubs+indID,1} = squeeze(nanmean(linFit_2_30_EC(idx1f(indID),channels),2));
    % add entropy info
    dataForPLS{(indCond-1)*numSubs+indID,2} = squeeze(nanmean(nanmean(mseMerged{indGroup, 2}.MSEVanilla(idxentropy(indID),channels, 1:3),2),3));
    dataForPLS{(indCond-1)*numSubs+indID,3} = squeeze(nanmean(nanmean(mseMerged{indGroup, 1}.MSEVanilla(idxentropy(indID),channels, 1:3),2),3));
    dataForPLS{(indCond-1)*numSubs+indID,4} = squeeze(nanmean(nanmean(mseMerged{indGroup, 2}.MSEVanilla(idxentropy(indID),channels, 1:3)-mseMerged{indGroup, 1}.MSEVanilla(idxentropy(indID),channels, 1:3),2),3));
    dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

% prepare ouput for PLS file

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

for indSub = 1:numel(groupfiles)
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

for indSub = 1:numel(groupfiles_mean)
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d %d %d %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1},...
        Fillin.VALUE{indSub, 2}, Fillin.VALUE{indSub, 3}, Fillin.VALUE{indSub, 4});
end

for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1});
end

figure; scatter([Fillin.VALUE{:, 1}], [Fillin.VALUE{:, 2}])