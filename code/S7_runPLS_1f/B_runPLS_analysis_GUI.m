clear all; clc; restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/';
pn.plstoolbox   = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/pls/']; addpath(genpath(pn.plstoolbox));

cd([pn.root, 'B_data/SD_STSWD_v1/']);

% batch_plsgui('BehavPLS_entropy_1f_SDBOLD_BfMRIanalysis.txt')
% batch_plsgui('BehavPLS_1group_entropy_1f_SDBOLD_BfMRIanalysis.txt')
% 
% batch_plsgui('BehavPLS_entropy_1f_MeanBOLD_BfMRIanalysis.txt')

plsgui


