function S3_commonGMvoxels_STSWD()
% Identify common GM coordinates to constrain analyses to.
%   nii_coords                    | non-NaN voxels across subjects
%   GM_coords                     | MNI GM mask voxels
%   final_coords                  | non-NaN MNI GM voxels
%   nii_nonZero                   | non-NaN, non-zero-power voxels across subjects
%   final_coords_withoutZero      | non-NaN, non-zero-power GM voxels across subjects

% PLS requires exlusively non-NaN voxels. GM voxels and non-Zero-power is
% optional, however may be recommended. Note that by focussing on
% non-Zero-power voxels, each subject contributes an identical amount of
% samples (N) to the analysis. Non-Zero-power voxels are usually located
% outside the brain and may inter-individually differ in number depending 
% on the coregistration with MNI. 

% Folder 'VoxelOverlap' has to be created manually and GM MNI mask has to
% be shifted into this directory.

% 171220 | adapted from SW, JR by JQK
% 180223 | adapted for STSWD

%% paths & setup

    BASEPATH    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
    DATAPATH    = [BASEPATH, 'analyses/C_PLS/B_data/VoxelOverlap/'];
    NIIPATH     = [BASEPATH, 'analyses/C_PLS/B_data/BOLDin/'];
    addpath(genpath([BASEPATH,'analyses/C_PLS/T_tools/NIFTI_toolbox/']));

    % N = 43 (1126 has no Rest data)
    IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};
    
    RunID={'rest'};

%% get grey matter coordinates based on MNI GM mask

    GM_coords = load_nii([DATAPATH, 'GM_MNI3mm_mask.nii']); % JQK: mask manually copied from EyeMem directory 
    GM_coords = reshape(GM_coords.img, [],1);
    GM_coords = find(GM_coords);

%% identify non-NaN voxels & non-zero power voxels

    nii_coords=1:60*72*60 ; % 3mm
    nii_nonZero=nii_coords;

    for indID = 1:numel(IDs)
        for indRun = 1:numel(RunID)
            NIINAME=([IDs{indID} '_' RunID{indRun} '_feat_detrended_bandpassed_manualdenoise_MNI3mm']);
            coords=load_nii([NIIPATH NIINAME '.nii']);
            coords=reshape(coords.img,[],coords.hdr.dime.dim(5));
            coords(1:100,:) = NaN;
            coords=std(coords,[],2); % JQK Hack: std will reveal voxels with any NaN across time as well as zero power
            coords_noNaN = find(~isnan(coords));
            nii_coords=intersect(nii_coords,coords_noNaN);
            % find non-zero power voxels across subjects
            coords_nonZero = find(~isnan(coords) & coords~=0);
            nii_nonZero=intersect(nii_nonZero,coords_nonZero);
            clear coords
            disp (['Done with ' RunID{indRun}])
        end
        disp (['Done with ' IDs{indID}])
    end

%% indices of non-NaN GM voxels

    final_coords = intersect(nii_coords, GM_coords);                            % non-NaN GM voxels across subjects
    final_coords_withoutZero = intersect(nii_nonZero, GM_coords);               % non-NaN & non-zero GM voxels across subjects

%% save coordinate mat

    save([DATAPATH, 'coords.mat'] ,'nii_coords', 'final_coords', 'GM_coords', 'final_coords_withoutZero', 'nii_nonZero');

end
