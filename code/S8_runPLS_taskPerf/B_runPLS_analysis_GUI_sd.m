clear all; clc; restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.plsroot      = [pn.root, 'analyses/B4_PLS_preproc2/'];
%pn.plstoolbox   = [pn.root, 'analyses/B4_PLS_preproc2/T_tools/PLS_LNDG2018/']; addpath(genpath(pn.plstoolbox));
pn.plstoolbox   = [pn.root, 'analyses/B4_PLS_preproc2/T_tools/pls/']; addpath(genpath(pn.plstoolbox));

cd(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/C_PLS/B_data/SD_STSWD_v1/']);

%batch_plsgui('behavPLS_STSWD_SD_YA_OA_2Group_v3_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
